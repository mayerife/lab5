#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct {        // заголовок растрового изображения занимает 54 байта
    char unused[18];
    char width[4];
    char height[4];
    char other[28];
} headerStr;

typedef struct {        // цвет пикселя
    int R, G, B;
} pixel;

typedef struct{         // размер карты (файла)
    int height, width;
} bitmapStr;


int bytesToInt(unsigned char *ch) {      // нужно для высоты и ширины, на которые выделено по 4 байта
    return (int)ch[3] << 24 | (int)ch[2] << 16 | (int)ch[1] << 8 | (int)ch[0];    // сдвиг битов влево, | - битовое "или"
}

int isAlive(pixel cell) {
    if (cell.R == 0 && cell.G == 0 && cell.B == 0) // живая, если закрашена черным
        return 1;
    return 0;
}

int getCell(int x, int y, int w, int h, int **cell) {   // достаем расположение клеточки
    x = (x + w) % w;       // благодаря этому работает тор
    y = (y + h) % h;
    return cell[y][x];
}

int getNeighbours (int x, int y, int **cells, int w, int h) {  // считаем соседей
    return getCell(x - 1, y + 1, w, h, cells) + getCell(x, y + 1, w, h, cells) + getCell(x + 1, y + 1, w, h, cells) +
           getCell(x - 1, y, w, h, cells) + getCell(x + 1, y, w, h, cells) + getCell(x - 1, y - 1, w, h, cells) +
           getCell(x, y - 1, w, h, cells) + getCell(x + 1, y - 1, w, h, cells);
}

void gameOfLife(int **field, int width, int height) {
    int neighCount;

    int **temp = (int **)malloc(sizeof(int*) * height);   // создаем временный двумерный массив-поле
    for (int i = 0; i < height; i++)
        temp[i] = (int*)malloc(width * sizeof(int));

    for (int y = 0; y < height; y++)
        for (int x = 0; x < width; x++) {
            neighCount = getNeighbours(x, y, field, width, height);  // проходим по массиву и достаем количество соседей
            if (field[y][x] == 1) {
                if (neighCount < 2 || neighCount > 3)  // по правилам умирает из-за одиночества или перенаселенности
                    temp[y][x] = 0;
                else
                    temp[y][x] = 1;   // иначе остается
            }

            else {
                if (neighCount == 3){   // в пустой клетке с 3-мя соседями зарождается жизнь
                    temp[y][x] = 1;}
                else{
                    temp[y][x] = 0;}
            }
        }

    for (int y = 0; y < height; y++)
        for (int x = 0; x < width; x++)
            field[y][x] = temp[y][x];     // переносим из временного массива всё в основное поле

    for (int i = 0; i < height; i++)
        free(temp[i]);
    free(temp);
}


// перевод int в строку
char* itoa(int num) {
    int size = 0;
    int t = num;
    while(t / 10 != 0) {  // находим размер числа
        t = t / 10;
        size++;
    }
    static char ret[64];  // создаем чар
    size++;
    ret[size] = '\0';     // последний символ (null)
    t = num;
    int i = size - 1;
    while(i >= 0) {       // переносим с конца
        ret[i] = (t % 10) + '0';
        t = t / 10;
        i--;
    }
    return ret;
}


int main(int argc, char* argv[]) {
    FILE* fin, *fout;
    int maxIter = 1000, dumpFreq = 1;   // поставила такое макс количество поколений, чтобы не сломать ноут
    char* dirname;
    pixel** pixels;
    int **oldGen;
    bitmapStr bitmap;
    headerStr header;

    if (argc < 5 || argc > 9) {          // непонятное кол-во аргументов
        printf("\nInvalid number of arguments!\n");
        return 1;
    }

    if (strcmp(argv[1],"--input") || strcmp(argv[3],"--output")) {   // не нашли инпут и аутпут
        printf("\nWrong format!\n");
        return 1;
    }

    fin = fopen(argv[2], "rb");   // чтение бинарного файла
    if (!fin){
        printf("\nCan't open this file!\n");
        return 1;
    }
    dirname = argv[4];

    if (argv[5] != NULL) {        // установка новых макситер и дампфрек вместо заданных по умолчанию
        if (!(strcmp(argv[5], "--max_iter")))
            maxIter = atoi(argv[6]);
        else if (!(strcmp(argv[5], "--dump_freq")))
            dumpFreq = atoi(argv[6]);
        if (argv[7] != NULL) {
            if (!(strcmp(argv[7], "--max_iter")))
                maxIter = atoi(argv[8]);
            else if (!(strcmp(argv[7], "--dump_freq")))
                dumpFreq = atoi(argv[8]);
        }
    }

    printf("\ndirname: %s\nmax iter: %d\ndump freq: %d\n", dirname, maxIter, dumpFreq);

// чтение исходника

    fread(&header, 1, 54,  fin);    // читаем заголовок файла и достаем размер
    bitmap.height = bytesToInt(header.height);  // переводим в инт
    bitmap.width = bytesToInt(header.width);
    printf("height: %ld, width: %ld\n\n", bitmap.height, bitmap.width, bitmap.height* bitmap.width);

    pixels = (pixel**)malloc(bitmap.height * sizeof(pixel*));   // создаем массивчик с пикселями (их цветовыми хар-ристиками)
    for (int i = 0; i < bitmap.height; i++)
        pixels[i] = (pixel*)malloc(bitmap.width * sizeof(pixel));
    for (int i = bitmap.height - 1; i >= 0; i--)       // в бмп мы идем слева направо из нижнего левого угла карты
        for (int j = 0; j < bitmap.width; j++) {
            pixels[i][j].R = getc(fin);
            pixels[i][j].G = getc(fin);
            pixels[i][j].B = getc(fin);
        }

// создание старого поколения

    oldGen = (int **)malloc(bitmap.height * sizeof(int*));  // массивчик для нулей и единиц
    for (int i = 0; i < bitmap.height; i++) {
        oldGen[i] = (int *) malloc(bitmap.width * sizeof(int));
    }

    for (int i = 0; i < bitmap.height; i++)           // проверка на жизнь клетки и закидывание 1 либо 0 в массив
        for (int j = 0; j < bitmap.width; j++) {
            if ((isAlive(pixels[i][j])) == 1)
                oldGen[i][j] = 1;
            else
                oldGen[i][j] = 0;
        }

// GAME OF LIFE

    for (int i = 1; i <= maxIter; i++) {   // проходимся для каждого поколения
        char path[256];   // каждый подкаталог пути ограничен 256 символами
        unsigned char* newGen;

        gameOfLife(oldGen, bitmap.width, bitmap.height);

        if (i % dumpFreq != 0)    // создаем файл только если удовлетворяет частоте
            continue;

        strcpy(path, dirname);
        strcat(path, "/");
        strcat(path, itoa(i));
        strcat(path, ".bmp");    // сделали файл, где будет картиночка текущей ситуации

        fout = fopen(path, "wb");
        if (!fout) {
            printf("Can't create file!\n");
            return 1;
        }

        fwrite(&header, 1, 54, fout);   // пишем заголовок (не отличается от изначального)
        int n = 0;

        newGen = (unsigned char*)malloc(bitmap.width * bitmap.height * 3);  // массив с 3 байтиками для rgb каждой клетки

        for (int m = bitmap.height - 1; m >= 0; m--)   // проходим по каждой клетке
            for (int j = 0; j < bitmap.width; j++)
                for (int k = 0; k < 3; k++) {
                    if (oldGen[m][j] == 1)
                        newGen[n] = 0;     // красим черным, если жива
                    else
                        newGen[n] = 255;   // иначе красим белым
                    n++;
                }

        fwrite(newGen, 1, bitmap.width * bitmap.height * 3, fout); // результат заносим в файл
        fclose(fout);
        free(newGen);
    }

    for (int i = 0; i < bitmap.height; i++)
        free(oldGen[i]);
    free(oldGen);

    for (int i = 0; i < bitmap.height; i++)
        free(pixels[i]);

    free(pixels);
    printf("Completed successfully!\n\n");   // вуаля
    fclose(fin);
}
